package main

import (
	"fmt"
	"github.com/robinson/gos7"
	"log"
	"os"
	"time"
)
const (
	tcpDevice = "192.168.13.13"
	rack      = 0
	slot      = 1
)
func TCPClient() {
	handler := gos7.NewTCPClientHandler(tcpDevice, rack, slot)
	handler.Timeout = 200 * time.Second
	handler.IdleTimeout = 200 * time.Second
	handler.Logger = log.New(os.Stdout, "tcp: ", log.LstdFlags)
	handler.Connect()
	defer handler.Close()
	client := gos7.NewClient(handler)
	ClientTestAll(client)
}
func ClientTestAll(client gos7.Client) {
	address := 101
	start := 268
	size := 2
	buf := make([]byte, 255)
	err := client.AGReadDB(address, start, size, buf)
	if err != nil {
		fmt.Println(err.Error())
	}
	// result := binary.BigEndian.Uint16(results)
	var s7 gos7.Helper
	var result uint16
	s7.GetValueAt(buf, 0, &result)
	fmt.Println(result)
}

func main() {
	TCPClient();
}
