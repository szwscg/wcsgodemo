module wcsgodemo

go 1.16

require (
	github.com/robinson/gos7 v0.0.0-20201216125248-2dd72fe148d3
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07 // indirect
	golang.org/x/sys v0.0.0-20210330210617-4fbd30eecc44 // indirect
)
